package app

import "core:math/linalg"
import rl "vendor:raylib"

FULL_RELOAD :: #config(FULL_RELOAD, false)

WIDTH :: 1270
HEIGHT :: 720

Vec2 :: [2]f32

Point :: struct {
  position: Vec2,
  prev_position: Vec2,
  mass: f32,
  pinned: bool,
  sticks: [2]int,
  selected: bool
}

Stick :: struct {
  p1, p2: int,
  length: f32,
  isSelected: bool,
  active: bool
}

AppState :: struct {
  points: [dynamic]Point,
  sticks: [dynamic]Stick,
}

g: ^AppState

mouse := struct {
  position: Vec2,
  prev_position: Vec2,
  size: f32
} {
  position = {WIDTH / 2, HEIGHT / 2},
  prev_position = {WIDTH / 2, HEIGHT / 2},
  size = 40
}

new_point :: proc(position: Vec2, pinned: bool) -> Point {
  return {
    position = position,
    prev_position = position,
    mass = 0.5,
    pinned = pinned,
    sticks = [2]int{-1, -1},
  }
}

app_init :: proc() -> ^AppState {
  g = new(AppState)

  clothWidth := WIDTH - 30
  clothHeight := HEIGHT - 100
  clothStartX := WIDTH / 2 - clothWidth / 2
  clothStartY := (HEIGHT - clothHeight) / 2
  spacing := 19

  horizontal := int(clothWidth / spacing)
  vertical := int(clothHeight / spacing)

  for y in 0..<vertical {
    for x in 0..<horizontal {
      pinned := y == 0 && x % 2 == 0
      point := new_point({ f32(clothStartX + x * spacing), f32(clothStartY + y * spacing) }, pinned)


      if x > 0 {
        leftPoint := &g.points[len(g.points) - 1]

        length := linalg.distance(leftPoint.position, point.position)
        stick := Stick{p1 = len(g.points) - 1, p2 = len(g.points), length = length, active = true}
        append(&g.sticks, stick)

        point.sticks[0] = len(g.sticks) - 1
        leftPoint.sticks[0] = len(g.sticks) - 1
      }

      if y > 0 {
        index := x + (y - 1) * (horizontal)
        upPoint := &g.points[index]

        length := linalg.distance(point.position, upPoint.position)
        stick := Stick{p1 = index, p2 = len(g.points), length = length, active = true}
        append(&g.sticks, stick)

        point.sticks[1] = len(g.sticks) - 1
        upPoint.sticks[1] = len(g.sticks) - 1
      }

      append(&g.points, point)
    }
  }

  return g
}

main :: proc() {
  rl.InitWindow(WIDTH, HEIGHT, "Cloth simulator")
  defer rl.CloseWindow()
  rl.SetTargetFPS(144)
  app_init()
  defer free(g)

  for {
    if !app_update() {
      break
    }
  }
}

app_update :: proc() -> bool {
  if rl.WindowShouldClose() {
    return false
  }

  update()
  render()

  return true
}

update :: proc() {
  dt := rl.GetFrameTime()

  mouse.prev_position = mouse.position
  mouse.position = rl.GetMousePosition()
  mouse.size = clamp(mouse.size + rl.GetMouseWheelMove() * 2, 10, 200)

  update_points(dt)

  for _ in 0..<4 {
    apply_sticks_constraints()
  }

  apply_points_constraints()
}

render :: proc() {
  rl.BeginDrawing()
  rl.ClearBackground(rl.BLACK)

  sticksActive :: proc(p: Point) -> bool {
    sticksAssigned := p.sticks[0] != 0 && p.sticks[1] != 0
    if !sticksAssigned {
      return false
    }

    return g.sticks[p.sticks[0]].active && g.sticks[p.sticks[1]].active
  }

  for p in g.points {
    if sticksActive(p) {
      color :=  p.selected ? rl.RED : rl.YELLOW
      rl.DrawCircleV(p.position, 3, color)
    }
  }

  for &s in g.sticks {
    if !s.active {
      continue
    }

    p1 := g.points[s.p1]
    p2 := g.points[s.p2]

    color :=  p1.selected || p2.selected ? rl.DARKGRAY : rl.LIGHTGRAY

    rl.DrawLineEx(p1.position, p2.position, 2, color)
  }

  rl.EndDrawing()
}

update_points :: proc(dt: f32) {
  friction : f32 = 0.01
  drag := clamp(1 - friction, 0.01, 1)
  ddt := dt * dt
  k := ddt * drag
  force := Vec2{0, 500}
  elasticity : f32 = 10.0

  for &p in g.points {
    if p.pinned {
      continue
    }

    distance_to_mouse := linalg.distance(mouse.position, p.position)
    p.selected = distance_to_mouse < mouse.size

    if p.selected && rl.IsMouseButtonDown(.LEFT) {
      mouseDiff := mouse.position - mouse.prev_position

      mouseDiff.x = clamp(mouseDiff.x, -elasticity, elasticity)
      mouseDiff.y = clamp(mouseDiff.y, -elasticity, elasticity)

      p.prev_position = p.position - mouseDiff
    }

    if p.selected && rl.IsMouseButtonDown(.RIGHT) {
      for i in p.sticks {
        if i == -1 {
          continue
        }

        stick := &g.sticks[i]
        stick.active = false
      }
    }

    acceleration := force / p.mass
    v := p.position - p.prev_position
    p.prev_position = p.position

    p.position = p.position + v * drag + acceleration * k
  }
}

apply_points_constraints :: proc() {
  for &p in g.points {
    keep_in_view(&p)
  }
}

apply_sticks_constraints :: proc() {
  for &s in g.sticks {
    if !s.active {
      continue
    }

    p1 := &g.points[s.p1]
    p2 := &g.points[s.p2]

    diff := p2.position - p1.position
    length := linalg.length(diff)
    offset := (length - s.length) / length * 0.5

    if p1.pinned || p2.pinned {
      offset *= 2
    }

    if !p1.pinned {
      p1.position += offset * diff
    }
    if !p2.pinned {
      p2.position -= offset * diff
    }
  }
}

keep_in_view :: proc(p: ^Point) {
  v := p.position - p.prev_position

  if p.position.x < 0 {
    p.position.x = 0
    p.prev_position.x = v.x
  }
  if p.position.x > WIDTH {
    p.position.x = WIDTH
    p.prev_position.x = WIDTH + v.x
  }

  if p.position.y < 0 {
    p.position.y = 0
    p.prev_position.y = v.y
  }
  if p.position.y > HEIGHT {
    p.position.y = HEIGHT
    p.prev_position.y = HEIGHT + v.y
  }
}
